cmake_minimum_required(VERSION 3.10)
project(OASK)

set(CMAKE_CXX_STANDARD 17)

include_directories(src)
include_directories(.)

add_executable(OASK
        src/ElementaryTransformations/ElementaryTransformations.cpp
        tests/TestMain.cpp
        tests/Tests.cpp
#        main.cpp
        )