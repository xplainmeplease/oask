#include <iostream>
#include <algorithm>
#include "src/ElementaryTransformations/ElementaryTransformations.hpp"


void s_row(uint8_t* message, int columns, uint8_t* result)
{
    const int rows = 8;
    int shift = -1;
    uint8_t* shifted_table1 = new uint8_t[rows * columns];
    uint8_t* shifted_table2 = new uint8_t[rows * columns];
    for (int i = 0; i < rows * columns; i++)
    {
        shifted_table1[i] = message[i / 8 * 8 + 7 - i % 8];
    }
    for (int i = 0; i < rows; i++)
    {
        if (i % (rows / columns) == 0)
        {
            shift++;
        }
        for (int j = 0; j < columns; j++)
        {
            shifted_table2[i + ((j + shift) % columns) * rows] = shifted_table1[i + j * rows];
        }
    }
    for (int i = 0; i < rows * columns; i++)
    {
        result[i] = shifted_table2[i / 8 * 8 + 7 - i % 8];
    }
    delete[] shifted_table1;
    delete[] shifted_table2;
}

int32_t main()
{

//    uint8_t message0[16] = { 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 };
//    uint8_t message1[16] = { 0,1,2,3,12,13,14,15,8,9,10,11,4,5,6,7 };
//
//    uint8_t message0[16] = { 0x9A,0x2B,0x1E,0xAC,0x76,0xEE,0x89,0x1B,0x91,0x4A,0xCF,0x17,0x7C,0x98,0xDD,0x3D };
    uint64_t M_m_col[10][2] = {{0x9A2B1EAC7C98DD3D, 0x914ACF1776EE891B}};
//    uint8_t M_m_col[10][2] = {// [test_no][col][row]
//            { 0x9A, 0x2B, 0x1E, 0xAC, 0x7C, 0x98, 0xDD, 0x3D },{  0x91, 0x4A, 0xCF, 0x17, 0x76, 0xEE, 0x89, 0x1B }};// 0
//    uint8_t message1[16] = { 0x9A,0x2B,0x1E,0xAC,0x7C,0x98,0xDD,0x3D,0x91,0x4A,0xCF,0x17,0x76,0xEE,0x89,0x1B };
    uint8_t message_mixed[16] = { 0x16,0xce,0xde,0xe8,0xd9,0x99,0x0f,0x9e,0x25,0xb5,0x06,0xf0,0x42,0xd3,0xb3,0x05 };
    OASK::Transformations::Elementary::m_col(reinterpret_cast<const uint8_t*>(OASK::Transformations::Elementary::INITIALIZATION_VECTOR), reinterpret_cast<uint8_t*>(M_m_col[0]));
    std::cout << "mixed data:  ";
    for(int32_t i = 0; i < 1; ++i)
    {
        for(int j = 0; j < 16; ++j)
        std::cout << std::hex << static_cast<uint32_t>(reinterpret_cast<uint8_t*>(M_m_col[i])[j]) << " ";
    }
    std::cout << "\n";
    std::cout << "expected data: ";
    for(uint8_t i : message_mixed)
    {
        std::cout << std::hex <<  (int)i << " ";
    }

}


