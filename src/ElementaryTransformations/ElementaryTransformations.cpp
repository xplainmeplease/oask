#include <cstring>
#include <algorithm>

#include "ElementaryTransformations.hpp"
namespace OASK::Transformations::Elementary
{
    void xor_rkey(uint64_t* matrixRow, const uint64_t* key) noexcept
    {
        //MATRIX_ROW_BYTES / sizeof(uint64_t) == count of columns, which is 2
       for(uint32_t i = 0; i < MATRIX_ROW_BYTES / sizeof(uint64_t); ++i)
       {
           matrixRow[i] ^= key[i];
       }
    }

    void add_rkey(uint64_t* matrixRow, const uint64_t* key) noexcept
    {
        //MATRIX_ROW_BYTES / sizeof(uint64_t) == count of columns, which is 2
        for(uint32_t i = 0; i < 2; ++i)
        {
            matrixRow[i] = matrixRow[i] + key[i];
        }
    }

    void s_row(uint8_t* matrixRow) noexcept
    {
        // Оксана Анатольевна, эта функция наверное сплошной "костыль", я её писал "от обратного"
        // проанализировав входные и выходные данные(из приложений с тестами),
        // и поделив их на "квотеры"(блоки по 4 байта),
        // я получил, что на входе: {1,2,3,4}, а на выходе {1,4,3,2}.
        // дальше я написал циклический сдвиг вправо, где величина сдвига равна i * 128 / 512,
        // И если так же представить данные в виде квотеров - получим:
        // данные: 1 2 3 4 |
        // сдвиг:  0 1 2 3 |
        // То есть если пошагово представить сдвиги в соотв. с формулой, получим:
        // 1) 1 2 3 4 : shift = 0, 0<=i<4
        // 2) 1 2 3 4 : shift = 1, 4<=i<8. Делаем сдвиг: 4 1 2 3
        // 3) 4 1 2 3 : shift = 2, 8<=i<12. Делаем сдвиг: 3 4 1 2 => 2 3 4 1
        // 4) 2 3 4 1 : shift = 3, 12<=i<16. Делаем сдвиг 1 2 3 4 => 4 1 2 3 => 3 4 1 2
        // Тут я заметил, что выходные данные - это  данные с пункта 3) в обратом порядке
        // И для этого на каждой итерации отнимал от shift единицу, что бы к моменту, когда 12<=i<16
        // мой сдвиг был равен 2.
        // Я ещё слабо разобрался в криптографии, и не знаю для чего нужно ревёрсить порядок данных,
        // но решил с помощью std::swap_ranges() сделать обратный порядок блоков, таким образом получил
        // такие же данные, что и на выходе: 1 4 3 2
        const int32_t columnsCount = 2;
        const int32_t halfRowBytes = 8;
        const int32_t rowBytes = halfRowBytes * columnsCount;

        for (int32_t i = 0; i < rowBytes; ++i)
        {
            int32_t shift = i * 128 / 512 - 1;
            CyclicShiftAllBytesRight(matrixRow, rowBytes, shift);

        }
        std::swap_ranges(matrixRow,matrixRow+4,matrixRow+12);
        std::swap_ranges(matrixRow+4,matrixRow+8,matrixRow+8);

        // я ещё не разобрался для чего мне нужно менять uint64_t matrixRow[i][0] и uint64_t matrixRow[i][1]
        // (это выяснилось в процессе дебага)
        std::swap_ranges(matrixRow,matrixRow+8,matrixRow+8);
    }

    void m_col(const uint8_t* initialVector, uint8_t* matrixRow) noexcept
    {
        auto* tmpMatrixRow = new uint8_t[MATRIX_ROW_BYTES];
        uint8_t  initialVectorRowOffset = 0;

#if defined(__unix__) || defined(__unix) || (defined(__APPLE__) && defined(__MACH__))

        //это сделано для того, что бы программа корректно работала с матрицей uint64_t M_m_col[10][2]
        //решил ревёрсить тут, а не "отзеркаливать" входные данные
        /////////////////////////////////////////////////////////
        std::reverse(matrixRow, matrixRow+MATRIX_ROW_BYTES/2);
        std::reverse(matrixRow + MATRIX_ROW_BYTES/2, matrixRow+MATRIX_ROW_BYTES);
        ////////////////////////////////////////////////
#endif

        for(int32_t i = 0; i < MATRIX_ROW_BYTES; ++i)
        {
            //(i-8)*8, для того, что бы "сбрасывать сдвиг вектора, когда он достигнет initialVector[i][8]
            initialVectorRowOffset = (i<8) ? i*8 : (i-8)*8;

            tmpMatrixRow[i] = MultiplyInitializationVectorRowByMatrixColumnAsPolinomes(initialVector + initialVectorRowOffset,matrixRow + (i/8*8));
        }

        memcpy(matrixRow, tmpMatrixRow, MATRIX_ROW_BYTES);
        
#if defined(__unix__) || defined(__unix) || (defined(__APPLE__) && defined(__MACH__))
        //это сделано для того, что бы программа корректно работала с матрицей uint64_t M_m_col[10][2]
        //решил ревёрсить тут, а не "отзеркаливать" входные данные
        /////////////////////////////////////////////////////////
        std::reverse(matrixRow, matrixRow+MATRIX_ROW_BYTES/2);
        std::reverse(matrixRow + MATRIX_ROW_BYTES/2, matrixRow+MATRIX_ROW_BYTES);
        ////////////////////////////////////////////////
#endif

    }

    void CyclicShiftAllBytesRight(uint8_t* matrixRow, uint8_t rowBytes, int16_t shift) noexcept
    {
        if(shift > 0)
        {
            for(uint16_t i = 0; i < shift; ++i)
            {
                const uint8_t lastNumberOfTheRow = matrixRow[rowBytes - 1];
                for(uint8_t j = rowBytes - 1; j > 0; --j)
                {
                    matrixRow[j] = matrixRow[j-1];
                }
                matrixRow[0] = lastNumberOfTheRow;
            }
        }
    }

    uint8_t MultiplyPolinomes(uint8_t lhs, uint8_t rhs) noexcept
    {
        uint16_t shiftedRhs = rhs; // копия второго множителя, который сдвигается каждую итерацию умножения на 1 влево,
                                   // можно обойти без копирования, так как всё равно работаем с копией аргумента,
                                   // но для читабельности было решено оставить
        uint16_t resultPolinome {};
        while(lhs != 0)
        {
            if(lhs & 0b1) // проверка LSB(less significant bit), в случе если 1 - то сохраняем результат с помощью XOR'a
            {
                resultPolinome ^= shiftedRhs;
            }
            shiftedRhs <<= 1; // второй множитель сдвигается независимо от того, что в LSB хранится(0 или 1)
            (shiftedRhs > 0xff) ? shiftedRhs ^= BASE_POLINOME : shiftedRhs; // как только сдигаемый множитель выходит за рамки 0xff -
                                                                            // сразу срезаем ненужную часть
            lhs >>= 1;
        }
        return resultPolinome;
    }

    uint8_t MultiplyInitializationVectorRowByMatrixColumnAsPolinomes(const uint8_t* initializationVectorRow, const uint8_t* matrixRow) noexcept
    {
        uint8_t result {};
        for(uint8_t i = 0; i < 8; ++i)
        {
            auto tmp = MultiplyPolinomes(initializationVectorRow[i], matrixRow[i]);
            result ^= tmp;
        }

        return  result;
    }

    void s_box(uint8_t* matrixRow,const uint8_t* key) noexcept
    {
        uint32_t keyOffset = 0;
        for (int32_t i = 0; i < MATRIX_ROW_BYTES; ++i)
        {
            keyOffset = (i % 4) * 256; // сдвиг по алфавиту

            matrixRow[i] = key[keyOffset + matrixRow[i]];
        }
    }

}