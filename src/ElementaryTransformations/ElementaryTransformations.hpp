#ifndef OASK_ELEMENTARYTRANSFORMATIONS_HPP
#define OASK_ELEMENTARYTRANSFORMATIONS_HPP

#include <cstdint>

namespace OASK::Transformations::Elementary
{
    constexpr uint16_t KEY_BITNESS = 128;
    constexpr uint16_t MATRIX_ROW_BYTES = KEY_BITNESS / 8;
    constexpr uint16_t BASE_POLINOME = 0x11d;
    constexpr uint8_t INITIALIZATION_VECTOR[8][8] = {{0x1, 0x1, 0x5, 0x1, 0x8, 0x6, 0x7, 0x4 },
                                                     {0x4, 0x1, 0x1, 0x5, 0x1, 0x8, 0x6, 0x7 },
                                                     {0x7, 0x4, 0x1, 0x1, 0x5, 0x1, 0x8, 0x6 },
                                                     {0x6, 0x7, 0x4, 0x1, 0x1, 0x5, 0x1, 0x8 },
                                                     {0x8, 0x6, 0x7, 0x4, 0x1, 0x1, 0x5, 0x1 },
                                                     {0x1, 0x8, 0x6, 0x7, 0x4, 0x1, 0x1, 0x5 },
                                                     {0x5, 0x1, 0x8, 0x6, 0x7, 0x4, 0x1, 0x1 },
                                                     {0x1, 0x5, 0x1, 0x8, 0x6, 0x7, 0x4, 0x1 }};

    void xor_rkey(uint64_t* matrixRow, const uint64_t* key) noexcept;
    void add_rkey(uint64_t* matrixRow, const uint64_t* key) noexcept;
    void s_row(uint8_t* matrixRow) noexcept;
    void m_col(const uint8_t* initialVector, uint8_t* matrixRow) noexcept;
    void s_box(uint8_t* matrixRow,const uint8_t* key) noexcept;

    //additional functions
    void CyclicShiftAllBytesRight(uint8_t* matrixRow, uint8_t rowBytes, int16_t shift) noexcept;

    uint8_t MultiplyPolinomes(uint8_t lhs, uint8_t rhs) noexcept;
    uint8_t MultiplyInitializationVectorRowByMatrixColumnAsPolinomes(const uint8_t* initializationVectorRow, const uint8_t* matrixRow) noexcept;
}



#endif //OASK_ELEMENTARYTRANSFORMATIONS_HPP
