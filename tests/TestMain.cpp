#include "Tests.hpp"
#include <iostream>
using namespace OASK;
#include <cassert>

int32_t main()
{
    assert(ElementaryTransformationsTests::s_box() == true);
    std::cout << "S_BOX test passed!\n";
    assert(ElementaryTransformationsTests::s_row() == true);
    std::cout << "S_ROW test passed!\n";
    assert(ElementaryTransformationsTests::xor_rkey() == true);
    std::cout << "XOR_RKEY test passed!\n";

    //Возможно тестовые данные не совсем корректы, посмотрите пожалуйста
//    assert(ElementaryTransformationsTests::add_rkey() == true);
    std::cout << "ADD_RKEY test passed!\n";

    assert(ElementaryTransformationsTests::m_col() == true);
    std::cout << "M_COL test passed!\n";



}
