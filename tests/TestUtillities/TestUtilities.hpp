#ifndef OASK_TESTUTILITIES_HPP
#define OASK_TESTUTILITIES_HPP

#include "s_box.hpp"
#include "s_row.hpp"
#include "s_keys.hpp"
#include "m_col.hpp"
#include "xor_rkey.hpp"
#include "add_rkey.hpp"

#endif //OASK_TESTUTILITIES_HPP
