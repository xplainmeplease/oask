#ifndef OASK_ADD_RKEY_HPP
#define OASK_ADD_RKEY_HPP

namespace TestUtilities::ADD_RKEYS
{
//m-ichnoe gammirovanie
    uint64_t M_add_rkey[4][2] = {{0x1011121314151617, 0x18191A1B1C1D1E1F},
                                 {0x2A996BD4E2BFE707, 0xEBBDF763CBFA64A5},
                                 {0x2021222324252627, 0x28292A2B2C2D2E2F},
                                 {0x95CD566091D32765, 0xB72653E17180F381}};

    uint64_t RK_add_rkey[4][2] = {{0x16505E6B9B3AB1E6, 0x865B77DCE082A0F4},
                                  {0xB8AA879A2086A66D, 0xD80A9872E25CD2B0},
                                  {0x57C816EB3F7E12DE, 0xED2C6B56E6B5BE1A},
                                  {0x2BBFF92FF9794546, 0xB343FADB28A0D1D4}};

    uint64_t C_add_rkey[4][2] = {{0x2661707EAF4FC7FD, 0x9E7491F7FC9FBE13},
                                 {0x81BF1C7D779BAC20, 0xE1C9EA39B4D2AD06},
                                 {0x77E9380E64A33805, 0x1556958112E3EC49},
                                 {0x58EC3E091000158A, 0x1148F7166F334F14}};

    uint64_t C1_sub_rkey[4][2] = {{0x1F1E1D1C1B1A1918, 0x1716151413121110},
                                  {0x0,                0x0},
                                  {0x0,                0x0},
                                  {0x0,                0x0}};

    uint64_t RK1_sub_rkey[4][2] = {{0x45D32764EB4B669E, 0xD8A3B2E73888CC77},
                                   {0x0,                0x0},
                                   {0x0,                0x0},
                                   {0x0,                0x0}};

    uint64_t M1_sub_rkey[4][2] = {{0xDA4AF5B72FCEB279, 0x3F72622CDA894498},
                                  {0x0,                0x0},
                                  {0x0,                0x0},
                                  {0x0,                0x0}};

}

#endif //OASK_ADD_RKEY_HPP
