#ifndef OASK_S_ROW_HPP
#define OASK_S_ROW_HPP

#include <cstdint>

namespace TestUtilities::S_ROW
{
// elementarnoe preobrazovanie perestanovki
    uint64_t M_s_row[10][2] = {{0x9A2B1EAC76EE891B, 0x914ACF177C98DD3D},
                               {0x81D13FB27562EDE1, 0xEA4B5542BFD1F76F},
                               {0x8FDA86338F6A9C7A, 0xBEA63AB2ED4D5139},
                               {0x249A648F714775DF, 0xE70E2C22E0FE9F0F},
                               {0xFA64888131B1FA10, 0x4244C60765ED68AD},
                               {0xC5ED3FA44D7990FE, 0x89EE6CDA99647C57},
                               {0x1DC39F955397E9F4, 0x146C93B7F3486EE0},
                               {0x9AEC1045C38C3593, 0xBBA1164CD70D8003},
                               {0x970380C8B39E909A, 0x2C7CF1480055E5E5},
                               {0xFF661201789C581D, 0x0374D34D5983C453}};

    uint64_t C_s_row[10][2] = {{0x9A2B1EAC7C98DD3D, 0x914ACF1776EE891B},
                               {0x81D13FB2BFD1F76F, 0xEA4B55427562EDE1},
                               {0x8FDA8633ED4D5139, 0xBEA63AB28F6A9C7A},
                               {0x249A648FE0FE9F0F, 0xE70E2C22714775DF},
                               {0xFA64888165ED68AD, 0x4244C60731B1FA10},
                               {0xC5ED3FA499647C57, 0x89EE6CDA4D7990FE},
                               {0x1DC39F95F3486EE0, 0x146C93B75397E9F4},
                               {0x9AEC1045D70D8003, 0xBBA1164CC38C3593},
                               {0x970380C80055E5E5, 0x2C7CF148B39E909A},
                               {0xFF6612015983C453, 0x0374D34D789C581D}};

    uint64_t C1_s_row[10][2] = {{0xAFF9B83FCDB4966C, 0xB66A08CEB5CB2EAD},
                                {0x57462621E191A038, 0x1BF38C63CF30B906},
                                {0x9642B19B65CF2889, 0xBDE6680A472ACF71},
                                {0xA6CDDEE3A0583E57, 0xAC8E7CD65D9B2B3F},
                                {0x8D530B8B7402299F, 0x3A503F7DF2AB32C7},
                                {0x401954D8D158D58A, 0x39F7F8FC5629A7BD},
                                {0x4A97D857436E3AE9, 0x86CA521D43136ED5},
                                {0x9622BE6806AF0D7E, 0xDED06D2B82536AF3},
                                {0xAA4DF979D644A69F, 0xCA4C64030DEF4808},
                                {0xD85F982C138E2146, 0x5D9E7EFE81F940F3}};

    uint64_t M1_s_row[10][2] = {{0xAFF9B83FB5CB2EAD, 0xB66A08CECDB4966C},
                                {0x57462621CF30B906, 0x1BF38C63E191A038},
                                {0x9642B19B472ACF71, 0xBDE6680A65CF2889},
                                {0xA6CDDEE35D9B2B3F, 0xAC8E7CD6A0583E57},
                                {0x8D530B8BF2AB32C7, 0x3A503F7D7402299F},
                                {0x401954D85629A7BD, 0x39F7F8FCD158D58A},
                                {0x4A97D85743136ED5, 0x86CA521D436E3AE9},
                                {0x9622BE6882536AF3, 0xDED06D2B06AF0D7E},
                                {0xAA4DF9790DEF4808, 0xCA4C6403D644A69F},
                                {0xD85F982C81F940F3, 0x5D9E7EFE138E2146}};
}

#endif //OASK_S_ROW_HPP
