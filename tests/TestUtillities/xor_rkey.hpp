#ifndef OASK_XOR_RKEY_HPP
#define OASK_XOR_RKEY_HPP

#include <cstdint>

namespace TestUtilities::XOR_RKEYS
{
// dvoichnoe gammirovanie - xor_rkey
    uint64_t M_xor_rkey[8][2] = {{0x16CEDEE8D9990F9E, 0x25B506F042D3B305},
                                 {0x32F172C7E2D2E1C9, 0x3B4D13958FBCE28D},
                                 {0x044E672502E945D3, 0x13F24197773D4547},
                                 {0x73EC521DA9BAF977, 0x7A44212456FE0215},
                                 {0x5DCBF7EE9765340D, 0x700AB86C7E3CFCBC},
                                 {0x44B90F555C682F06, 0x8FDB6918B7108ED5},
                                 {0x1073FCB6ECF68F31, 0xBCB77E752220169C},
                                 {0x375DCC2C800C45F6, 0x1DD9C471E522E0A6}};

    uint64_t RK_xor_rkey[8][2] = {{0xE6865B77DCE082A0, 0xF416505E6B9B3AB1},
                                  {0x7E70876EAE498476, 0x8AAAA00A7C93EC42},
                                  {0x768AAAA00A7C93EC, 0x427E70876EAE4984},
                                  {0x45CED4C51E9140F5, 0x3E7276820F0BD9FE},
                                  {0xF53E7276820F0BD9, 0xFE45CED4C51E9140},
                                  {0x62515F66320560C4, 0xB18C77EE227900C4},
                                  {0x0A9872E25CD2B0B8, 0xAA879A2086A66DD8},
                                  {0xB8AA879A2086A66D, 0xD80A9872E25CD2B0}};

    uint64_t C_xor_rkey[8][2] = {{0xF048859F05798D3E, 0xD1A356AE294889B4},
                                 {0x4C81F5A94C9B65BF, 0xB1E7B39FF32F0ECF},
                                 {0x72C4CD850895D63F, 0x518C311019930CC3},
                                 {0x362286D8B72BB982, 0x443657A659F5DBEB},
                                 {0xA8F58598156A3FD4, 0x8E4F76B8BB226DFC},
                                 {0x26E850336E6D4FC2, 0x3E571EF695698E11},
                                 {0x1AEB8E54B0243F89, 0x1630E455A4867B44},
                                 {0x8FF74BB6A08AE39B, 0xC5D35C03077E3216}};

    uint64_t C1_xor_rkey[8][2] = {{0x17AF69BA9A0547EB, 0x259BC23A8813BDB0},
                                  {0xDAB1C98CEC7066C5, 0x8D43F8862B4EF241},
                                  {0x54107A685E80915F, 0xB3ADDBC4598B986C},
                                  {0xAADB3DEA9C4912BA, 0xE0526D4AA7239CFC},
                                  {0xEFD471994146C8AF, 0x93D085E6AB2A8F91},
                                  {0x9956F66AFEFEE91F, 0x1F33FCDD34235119},
                                  {0x4B7723FC0106751B, 0x685FC09B01C1B348},
                                  {0x5426C500CFD40932, 0x9BE384C703D7B681}};


    uint64_t RK1_xor_rkey[8][2] = {{0x2479F950B52187E2, 0xAE8BD65CCC7452D0},
                                   {0x8BD65CCC7452D024, 0x79F950B52187E2AE},
                                   {0x028F414771A7C56F, 0x54CA78E33B34F568},
                                   {0xCA78E33B34F56802, 0x8F414771A7C56F54},
                                   {0x7F6553DB7E6FD3F8, 0x7EF21749B0A405C2},
                                   {0xF21749B0A405C27F, 0x6553DB7E6FD3F87E},
                                   {0xD802781E471AB0CA, 0xFA9845CC2A6036C0},
                                   {0x9845CC2A6036C0D8, 0x02781E471AB0CAFA}};

    uint64_t M1_xor_rkey[8][2] = {{0x33D690EA2F24C009, 0x8B1014664467EF60},
                                  {0x516795409822B6E1, 0xF4BAA8330AC910EF},
                                  {0x569F3B2F2F275430, 0xE767A32762BF6D04},
                                  {0x60A3DED1A8BC7AB8, 0x6F132A3B00E6F3A8},
                                  {0x90B122423F291B57, 0xED2292AF1B8E8A53},
                                  {0x6B41BFDA5AFB2B60, 0x7A6027A35BF0A967},
                                  {0x93755BE2461CC5D1, 0x92C785572BA18588},
                                  {0xCC63092AAFE2C9EA, 0x999B9A8019677C7B}};
}
#endif //OASK_XOR_RKEY_HPP
