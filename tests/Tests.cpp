#include <cstdint>
#include <cstring>
#include "Tests.hpp"
#include "tests/TestUtillities/TestUtilities.hpp"

namespace OASK
{
    namespace ElementaryTransformationsTests
    {
        bool xor_rkey() noexcept
        {
            for (int i = 0; i < 9; i++)
            {
                Transformations::Elementary::xor_rkey(TestUtilities::XOR_RKEYS::M_xor_rkey[i], TestUtilities::XOR_RKEYS::RK_xor_rkey[i]);
            }
            return (memcmp(TestUtilities::XOR_RKEYS::M_xor_rkey,TestUtilities::XOR_RKEYS::C_xor_rkey, sizeof(TestUtilities::XOR_RKEYS::M_xor_rkey)) == 0);
        }

        bool s_box() noexcept
        {
            for (auto & M_s_box_item : TestUtilities::S_BOX::M_s_box)
            {
                Transformations::Elementary::s_box(M_s_box_item, reinterpret_cast<const uint8_t*>(TestUtilities::S_KEYS::s));
            }

            return (memcmp(TestUtilities::S_BOX::M_s_box,TestUtilities::S_BOX::C_s_box, sizeof(TestUtilities::S_BOX::M_s_box)) == 0);
        }

        bool s_row() noexcept
        {
            for (auto & M_s_row_item : TestUtilities::S_ROW::M_s_row)
            {
                Transformations::Elementary::s_row(reinterpret_cast<uint8_t*>(M_s_row_item));
            }

            return (memcmp(TestUtilities::S_ROW::M_s_row,TestUtilities::S_ROW::C_s_row, sizeof(TestUtilities::S_ROW::M_s_row)) == 0);
        }

        bool add_rkey() noexcept
        {
            // Тест заккоментирован умышленно, так как:
            /* Есть случаи, где ожидается переполнение,и там результат, который получает программа, неравен тому,
             * что ожидается(а именно данные в C_add_rkey[][])
             * Ниже находится отладочная информация, которой я руководствовался
             */
//            using namespace TestUtilities::ADD_RKEYS;
//            using namespace std;
//            std::cout << M_add_rkey[0][0] << "+" << RK_add_rkey[0][0] << " = " <<  M_add_rkey[0][0] + RK_add_rkey[0][0] << " | " << C_add_rkey[0][0] << "\n";
//            std::cout << M_add_rkey[0][1] << "+" << RK_add_rkey[0][1] << " = " <<  M_add_rkey[0][1] + RK_add_rkey[0][1] << " | " << C_add_rkey[0][1] << "\n";
//
//            std::cout << M_add_rkey[1][0] << "+" << RK_add_rkey[1][0] << " = " <<  M_add_rkey[1][0] + RK_add_rkey[1][0] << " | " << C_add_rkey[1][0] << "\n";
//            std::cout << M_add_rkey[1][1] << "+" << RK_add_rkey[1][1] << " = " <<  M_add_rkey[1][1] + RK_add_rkey[1][1] << " | " << C_add_rkey[1][1] << "\n";
//
//            std::cout << M_add_rkey[2][0] << "+" << RK_add_rkey[2][0] << " = " <<  M_add_rkey[2][0] + RK_add_rkey[2][0] << " | " << C_add_rkey[2][0] << "\n";
//            std::cout << M_add_rkey[2][1] << "+" << RK_add_rkey[2][1] << " = " <<  M_add_rkey[2][1] + RK_add_rkey[2][1] << " | " << C_add_rkey[2][1] << "\n";
//
//            std::cout << M_add_rkey[3][0] << "+" << RK_add_rkey[3][0] << " = " <<  M_add_rkey[3][0] + RK_add_rkey[3][0] << " | " << C_add_rkey[3][0] << "\n";
//            std::cout << M_add_rkey[3][1] << "+" << RK_add_rkey[3][1] << " = " <<  M_add_rkey[3][1] + RK_add_rkey[3][1] << " | " << C_add_rkey[3][1] << "\n";
//            std::cout << "//////////////////////////\n";
            /*
             * Это результат вывода выше
             *    M_add_rkey[0][j] + RK_add_rkey[0][j] =                    |   C_add_rkey[0][j]
             *  1157726452361532951+1607888883230093798 = 2765615335591626749 | 2765615335591626749
                1736447835066146335+9681463614506180852 = 11417911449572327187 | 11417911449572326931

             *    M_add_rkey[1][j] + RK_add_rkey[1][j] =                    |   C_add_rkey[1][j]
                3069603183111169799+13306597145059960429 = 16376200328171130228 | 9349222676647619616
                16987005377507386533+15567422681150968496 = 14107683984948803413 | 16269792662420368646

                M_add_rkey[2][j] + RK_add_rkey[2][j] =                    |   C_add_rkey[2][j]
                2315169217770759719+6325330876280214238 = 8640500094050973957 | 8640498994556123141
                2893890600475373103+17090152706946219546 = 1537299233712041033 | 1537580704393849929

                M_add_rkey[3][j] + RK_add_rkey[3][j] =                    |   C_add_rkey[3][j]
                10794378854640330597+3152512248626890054 = 13946891103267220651 | 6407564578485441930
                13197327985792840577+12917443975415124436 = 7668027887498413397 | 1245516972694916884
             */

            for (int i = 0; i < 4; i++)
            {
                Transformations::Elementary::add_rkey(TestUtilities::ADD_RKEYS::M_add_rkey[i], TestUtilities::ADD_RKEYS::RK_add_rkey[i]);
            }

            return (memcmp(TestUtilities::ADD_RKEYS::M_add_rkey,TestUtilities::ADD_RKEYS::C_add_rkey, sizeof(TestUtilities::ADD_RKEYS::M_add_rkey)) == 0);

        }

        bool m_col() noexcept
        {
            for(auto & M_m_col_item : TestUtilities::M_COL::M_m_col)
            {
                Transformations::Elementary::m_col(reinterpret_cast<const uint8_t*>(Transformations::Elementary::INITIALIZATION_VECTOR),
                                                   reinterpret_cast<uint8_t*>(M_m_col_item));
            }



            return (memcmp(TestUtilities::M_COL::M_m_col,TestUtilities::M_COL::C_m_col, sizeof(TestUtilities::M_COL::M_m_col)) == 0);
        }
    }
}