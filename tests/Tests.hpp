#ifndef OASK_TESTS_HPP
#define OASK_TESTS_HPP

#include "src/ElementaryTransformations/ElementaryTransformations.hpp"

namespace OASK
{
    namespace ElementaryTransformationsTests
    {
        bool xor_rkey() noexcept;
        bool s_box() noexcept;
        bool s_row() noexcept;
        bool add_rkey() noexcept;
        bool m_col() noexcept;

    }
}



#endif //OASK_TESTS_HPP
